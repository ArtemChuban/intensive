# Market

Микросервисное приложение для онлайн магазина

Состоит из трех микросервисов:
- frontend - React приложение
- backend - FastAPI приложение
- mongo_database - база данных MongoDB


Frontend состоит из трех страниц:
1. Главная страница - отображает все товары (изображение, название, цена)
2. Страница товара - отображает один определенный товар (изображение, название, цена, описание) и позволяет добавить этот товар в корзину
3. Корзина - отображает все добавленные в корзину товары (изображение, название, цена), общую сумму корзины, позволяет указать адрес доставки и сделать заказ после оплаты

## Полезные ссылки

- Frontend
    - [JavaScript](https://developer.mozilla.org/en-US/docs/Web/javascript)
    - [React](https://react.dev)
    - Примеры
        - [Wildberries](https://www.wildberries.ru)
        - [Ozon](https://www.ozon.ru)
        - [Яндекс Маркет](https://market.yandex.ru)
        - [AliExpress](https://aliexpress.ru)
        - [Сбер Маркет](https://sbermarket.ru)
        - [Lamoda](https://www.lamoda.ru)
- Backend
    - [Python](https://docs.python.org/3)
    - [FastAPI](https://fastapi.tiangolo.com)
    - [MongoDB](https://www.mongodb.com/docs)
    - [Pymongo](https://pymongo.readthedocs.io/en/stable/index.html)
- Misc
    - [Docker](https://docs.docker.com)
    - [Git](https://git-scm.com)
